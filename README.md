# À propos

Ce dépot a pour but de garder une trace, sous forme d'un fichier org, de l'avancement des modifications réalisées par certains membres de la [Cabale Informatique France (CIF)](https://fr.wikipedia.org/wiki/Projet:Informatique/Cabale_Informatique_France)
